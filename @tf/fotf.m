  function G1=fotf(G) 
  [n,d]=tfdata(G,'v'); Td=get(G,'ioDelay');
  i1=find(abs(n)<eps); i2=find(abs(d)<eps); 
  if length(i1)>0 & i1(1)==1, n=n(i1(1)+1:end); end 
  if length(i2)>0 & i2(1)==1, d=d(i2(1)+1:end); end 
  G1=fotf(d,length(d)-1:-1:0,n,length(n)-1:-1:0,Td);
