function [c,ceq] = mycon(x)

Kp=x(1);
Ki=x(2);
Kd=x(3);
L=x(4);
M=x(5);

c=[ -Kp
    -Ki
    -Kd
    Ki-200
    Kp-200
    Kd-10
    L+0.01
    0.1-M
    0.1-Kd];

ceq=0;