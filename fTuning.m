%%  Parameters
%Initialize Blocks... (Dokumentzidis et al.)
pbpk.k10=1.4913;
pbpk.k12=2.9522;
pbpk.k21=0.4854;
pbpk.a=0.5870;

%% Controller parameters
Kp=25;   %Controller's Static Gain
Ki=120;  %1/Ti
Kd=0.2;   %Td
L=-1; %Integrator's Order
M=0.8;  %Derivator's Order

%% ITAE with constraints
Kp=50.5197;
Ki=151.0551;
Kd= 0.063;
L=-0.9170;
M=0.7590;
[x_opt, fval, exitFlag] = fmincon(@my_obj2,[Kp Ki Kd L M]',[],[],[],[],[],[],@mycon);

%% ITAE
% INITIAL VALUES (Educated Guess)
options=optimset('Display','iter','TolFun',1e-3,'TolX',1e-3);

Kp=50.5197;
Ki=151.0551;
Kd= 0.0756;
L=-0.9170;
M=0.7590;
[x, fval, exitFlag]=fminsearch(@my_obj2,[Kp Ki Kd L M],options)

% Using the ITAE we found...
% optX_ = [50.5197  151.0551    0.0756   -0.9170    0.7590];
