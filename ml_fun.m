function f=ml_fun(a,b,x,n,eps0)
if nargin<5, eps0=eps; end 
if nargin<4, n=0; end, 
f=0; fa=1; j=0; 
while norm(fa,1)>=eps0
   fa=gamma(j+n+1)/gamma(j+1)/gamma(a*j+a*n+b)*x.^j;
   f=f+fa; j=j+1; 
end