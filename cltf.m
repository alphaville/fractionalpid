function mag = cltf(omega,pbpk, Kp, Ki, Kd, L, M)
%CLTF Transfer Function of the closed-llop system 
%with the fractional-PID controller

s=omega*1i;

controller_ = Kp + Ki * s^L + Kd * s^M;

numerator_ = s^pbpk.a+pbpk.k21;
denominator_ = s^(pbpk.a+1) + pbpk.k21*s + (pbpk.k12+pbpk.k10)*s^pbpk.a + ...
    pbpk.k10*pbpk.k21;

mag = abs(controller_)*abs(numerator_)/abs(denominator_+controller_*numerator_);