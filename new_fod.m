 function G=new_fod(r,N,wb,wh,b,d)
 if nargin==4, b=10; d=9; end
 mu=wh/wb; k=-N:N; w_kp=(mu).^((k+N+0.5-0.5*r)/(2*N+1))*wb;
 w_k=(mu).^((k+N+0.5+0.5*r)/(2*N+1))*wb; K=(d*wh/b)^r; 
 G=zpk(-w_kp',-w_k',K)*tf([d,b*wh,0],[d*(1-r),b*wh,d*r])
