   function y=my_obj1(x)
   assignin('base','Kp',x(1));
   assignin('base','Ki',x(2));
   assignin('base','Kd',x(3));
   assignin('base','L',x(4));
   assignin('base','M',x(5));
   [t,x,y1]=sim('c13fpid',[0 15]); y=y1(end);
 