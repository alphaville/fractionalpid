function H=bode(G,w)
if nargin==1, w=logspace(-4,4); end
j=sqrt(-1); H1=freqresp(j*w,G); H1=frd(H1,w);
P = bodeoptions;
P.Title.FontSize=12;
P.XLabel.FontSize=12;
P.YLabel.FontSize=12;
P.FreqUnits='rad/day';
if nargout==0,
    bode(H1,P);
else
    H=H1;
end
