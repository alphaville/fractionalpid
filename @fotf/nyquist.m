   function nyquist(G,w)
   if nargin==1, w=logspace(-4,4); end
   H=bode(G,w); nyquist(H);
