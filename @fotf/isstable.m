  function [K,alpha,err,apol]=isstable(G,a0)
  if G.ioDelay~=0, error('delay system cannot be assessed'); end
  if nargin==1, a0=0.01; end
  a=G.na; a1=fix(a/a0); n=gcd(a1(1),a1(2));
  for i=3:length(a1), n=gcd(n,a1(i)); end
  alpha=n*a0; a=fix(a1/n); b=G.a; c(a+1)=b; c=c(end:-1:1);
  p=roots(c); p=p(abs(p)>eps); err=norm(polyval(c,p));
  plot(real(p),imag(p),'x',0,0,'o')
  apol=min(abs(angle(p))); K=apol>alpha*pi/2;
  xm=xlim; xm(1)=0; line(xm,alpha*pi/2*xm)
  %line(xm,alpha*pi*xm) % for Li Yan
