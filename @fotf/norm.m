   function n=norm(G,eps0)
   j=sqrt(-1); dx=10; f0=0;
   if nargin==1, eps0=eps; end
   if nargin==2 & ~isfinite(eps0) %{\rm Hinf}
      f=@(x)[-abs(freqresp(j*x,G))];
      x=-fminsearch(f,0); n=abs(freqresp(j*x,G));
   else  %{\rm H2}
      f=@(x)freqresp(x,G).*freqresp(-x,G);
      while (1)
         n=quadgk(f,-dx*j,dx*j)/(2*pi*j);
         if abs(n-f0)<eps0 | n<f0, break; else, f0=n; dx=dx*1.2; 
   end, end, end
