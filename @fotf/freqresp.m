 function H1=freqresp(w,G)
 a=G.a; na=G.na; b=G.b; nb=G.nb; j=sqrt(-1); w1=w/j;
 nW=length(w);
 H1=zeros(nW,1);
 for i=1:nW
    P=b*(w(i).^nb.'); 
    Q=a*(w(i).^na.'); 
    H1(i)=P/Q; 
 end
 if G.ioDelay>0,
    A=abs(H1); B=angle(H1)-w1*G.ioDelay; H1=A.*exp(j*B);
 end
