   function G=feedback(F,H)
   F=fotf(F); H=fotf(H); na=[]; nb=[];
   if F.ioDelay==H.ioDelay
      b=kron(F.b,H.a); a=[kron(F.b,H.b), kron(F.a,H.a)];
      for i=1:length(F.b),
         nb=[nb F.nb(i)+H.nb]; na=[na,F.nb(i)+H.nb];
      end
      for i=1:length(F.a), na=[na F.na(i)+H.na]; end
      G=simple(fotf(a,na,b,nb,F.ioDelay));
   else, error('cannot handle blocks with different delays'); end
