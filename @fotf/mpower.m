   function G1=mpower(G,n)
   if n==fix(n), 
      if n>=0, G1=1; for i=1:n, G1=G1*G; end
      else, G1=inv(G^(-n)); end
      G.ioDelay=n*G.ioDelay;
   elseif length(G.a)*length(G.b)==1 & G.na==0 & G.nb==1,
     G1=fotf(1,0,1,n);
   else, error('mpower: power must be an integer.'); end
