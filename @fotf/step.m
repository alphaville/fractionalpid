   function y=step(G,t)
   y=fode_sol(G.a,G.na,G.b,G.nb,ones(size(t)),t);
   ii=find(t>G.ioDelay); lz=zeros(1,ii(1)-1);
   y=[lz, y(1:end-length(lz))];
