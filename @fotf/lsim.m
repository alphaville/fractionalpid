   function y=lsim(G,u,t)
   y=fode_sol(G.a,G.na,G.b,G.nb,u,t); 
   ii=find(t>G.ioDelay); lz=zeros(1,ii(1)-1);
   y=[lz, y(1:end-length(lz))]; 