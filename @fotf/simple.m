   function G=simple(G1)
   [a,n]=polyuniq(G1.a,G1.na); G1.a=a; G1.na=n; na=G1.na; 
   [a,n]=polyuniq(G1.b,G1.nb); G1.b=a; G1.nb=n; nb=G1.nb;
   nn=min(na(end),nb(end)); nb=nb-nn; na=na-nn;
   G=fotf(G1.a,na,G1.b,nb,G1.ioDelay);
   %{\rm local function \texttt{polyuniq} for collecting polynomial terms}
   function [a,an]=polyuniq(a,an)
   [an,ii]=sort(an,'descend'); a=a(ii); ax=diff(an); key=1;
   for i=1:length(ax)
      if ax(i)==0,
         a(key)=a(key)+a(key+1); a(key+1)=[]; an(key+1)=[];
      else, key=key+1; end
   end
