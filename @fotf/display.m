   function display(G)
   strN=polydisp(G.b,G.nb); strD=polydisp(G.a,G.na);
   nn=length(strN); nd=length(strD); nm=max([nn,nd]);
   disp([char(' '*ones(1,floor((nm-nn)/2))) strN]), ss=[];
   T=G.ioDelay; if T>0, ss=['exp(-' num2str(T) 's)']; end
   disp([char('-'*ones(1,nm)), ss]);
   disp([char(' '*ones(1,floor((nm-nd)/2))) strD])
   function strP=polydisp(p,np)
   P=''; [np,ii]=sort(np,'descend'); p=p(ii);
   for i=1:length(p),
      P=[P,'+',num2str(p(i)),'s^{',num2str(np(i)),'}'];
   end
   P=P(2:end); P=strrep(P,'s^{0}','');
   P=strrep(P,'+-','-'); P=strrep(P,'^{1}','');
   P=strrep(P,'+1s','+s');
   strP=strrep(P,'-1s','-s'); nP=length(strP);
   if nP>=2 & strP(1:2)=='1s', strP=strP(2:end); end
