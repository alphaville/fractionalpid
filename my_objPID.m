   function y=my_objISE(x)
  
   x
   assignin('base','Kp',x(1));
   assignin('base','Ki',x(2));
   assignin('base','Kd',x(3));
 
   [t,x,y1]=sim('kineticPID_PID',[0 0.5]); y=1000*y1(end);