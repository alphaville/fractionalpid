   function dy=caputo(t0,f,gam)
   m=floor(gam); a=gam-m; dy=0;
   if gam>0, syms t; fd=diff(f,t,m+1); else; a=-gam; end
   for t1=t0(2:end)
      if gam>0,
         f=@(x)subs(fd,t,x)./(t1-x).^a/gamma(1-a);
      else
         f=@(x)subs(f,t,x)./(t1-x).^(1-a)/gamma(a);
      end
      dy=[dy; quadl(f,0,t1)];
   end
