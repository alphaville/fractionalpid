%% Optimum System
Thor=3;
% optX_ = [50.5197  151.0551    0.0756   -0.9170    0.7590];
% J_star = 3.9713e-04;
Kp=50.5197;
Ki=151.0551;
Kd= 0.0756;
L=-0.9170;
M=0.7590;

pbpk_0 = pbpk;
warning('off');
figure(1239);
hold on;
for i=1:10
    pbpk.a = pbpk_0.a*(max([min([1+0.2*randn, 1.4]),0.7]));
    pbpk.a 
    disp(i);
    try,
        sim('kineticPID',[0 0.6]);
        plot(response.time(20:end),response.signals.values(20:end));
    end
end
grid on;
%%
[~,~,y1]=sim('kineticPID',[0 1]);
itae_opt=y1(end);
response_opt = response;
actuator_opt = actuator;


Kp=30;
[~,~,y1]=sim('kineticPID',[0 Thor]);
itae_Kp30=y1(end)
response_Kp30 = response;
actuator_Kp30 = actuator;

plot(response_Kp30.time,response_Kp30.signals.values,'b --');
plot(response.time,response.signals.values,'b --');

%% Plots
figure(1);
hold on;
plot(response_opt.time,response_opt.signals.values,'b --','LineWidth',2);
plot(actuator_opt.time,actuator_opt.signals.values,'b','LineWidth',2);

%% Bode Plots
B=[1, pbpk.k12];
nB=[pbpk.a, 0];

A=[1, pbpk.k21, pbpk.k12+pbpk.k10, pbpk.k10*pbpk.k21];
nA=[pbpk.a+1, 1, pbpk.a, 0];

w=logspace(-2,3);
G=fotf(A,nA,B,nB);


nC=[1 L M];
c=[Kp Ki Kd];
Gc=fotf(A,nA,B,nB);

Gol=Gc*G;
bode(G,w);hold on;%blue
bode(Gol,w);%green
grid on;
hold off;

w=logspace(-2,2);
Gcl=Gol/(1+Gol);
figure(2);
bode(Gcl,w);
grid on;

w=logspace(-3,2);
Gs=1/(1+Gc*G);
figure(3);
bode(Gs,w);
grid on;