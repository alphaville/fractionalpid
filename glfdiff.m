   function dy=glfdiff(y,t,gam)
   h=t(2)-t(1); dy(1)=0; y=y(:); t=t(:); w=1;
   for j=2:length(t), w(j)=w(j-1)*(1-(gam+1)/(j-1)); end
   for i=2:length(t), dy(i)=w(1:i)*[y(i:-1:1)]/h^gam; end
