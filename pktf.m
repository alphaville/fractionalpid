function [mag phi] = pktf(omega,pbpk)
%PKTF Transfer Function of the PK model

s=omega*1i;
numerator_ = s^pbpk.a+pbpk.k21;
denominator_ = s^(pbpk.a+1) + pbpk.k21*s + (pbpk.k21+pbpk.k10)*s^pbpk.a + ...
    pbpk.k10*pbpk.k21;
mag = abs(numerator_)/abs(denominator_);
phi = angle(numerator_/denominator_);