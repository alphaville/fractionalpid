function y=my_obj2(x)
pbpk.k10=1.4913;
pbpk.k12=2.9522;
pbpk.k21=0.4854;
pbpk.a=0.5870;
omega_h=1000;
x
assignin('base','Kp',x(1));
assignin('base','Ki',x(2));
assignin('base','Kd',x(3));
assignin('base','L', x(4));
assignin('base','M',x(5));
[~,~,y1]=sim('kineticPID',[0 0.5]);
itae = y1(end);
assignin('base','itae',itae);
clhf = cltf(omega_h,pbpk, x(1), x(2), x(3), x(4), x(5));
assignin('base','clhf',clhf);
sslf = sstf(0.01,pbpk,x(1), x(2), x(3), x(4), x(5));
assignin('base','sslf',sslf);
y = 300 * itae + clhf + sslf;