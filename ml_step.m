 function y=ml_step(a,b,t,eps0)
 a0=a(1); a1=a(2); a2=a(3); b1=b(1); b2=b(2);
 y=0; k=0; ya=1; a0=a0/a2; a1=a1/a2;
 if nargin==3, eps0=eps; end
 while max(abs(ya))>=eps0
     ya=(-1)^k/gamma(k+1)*a0^k*t.^((k+1)*b2).*...
         ml_fun(b2-b1,b2+b1*k+1,-a1*t.^(b2-b1),k,eps0);
     y=y+ya; k=k+1;
 end
 y=y/a2;
