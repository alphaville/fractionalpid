   function y=fode_sol(a,na,b,nb,u,t)
   h=t(2)-t(1); D=sum(a./[h.^na]); nT=length(t);
   vec=[na nb]; W=[]; D1=b(:)./h.^nb(:); nA=length(a);
   y1=zeros(nT,1); W=ones(nT,length(vec));
   for j=2:nT, W(j,:)=W(j-1,:).*(1-(vec+1)/(j-1)); end
   for i=2:nT,
      A=[y1(i-1:-1:1)]'*W(2:i,1:nA);
      y1(i)=(u(i)-sum(A.*a./[h.^na]))/D;
   end
   for i=2:nT, y(i)=(W(1:i,nA+1:end)*D1)'*[y1(i:-1:1)]; end
